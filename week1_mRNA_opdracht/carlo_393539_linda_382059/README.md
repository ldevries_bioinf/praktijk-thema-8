Authors: C. van Buiten, L. de Vries   
Date: 14 April 2020   
Version: Bio-informatica 2, praktijk opdracht 08   
 

# Project name 
Dynamic mRNA   

# Introduction
-

# Requirements  
Rstudio_1.2.5019            
R_3.5.2  
deSolve_1.28

# Manual 
The folder praktijk-thema-7 contains the files you use yo get the results. You start with running the ```package_installer.Rmd``` file, to install all the packages you need. After that, the script ```carlo_393539_linda_382059.Rmd``` can be run. Here you can look at the results, there is also a report this is the PDF file ```carlo_393539_linda_382059.pdf```. There is also a script ```carlo_393539_linda_382059.R``` this is only include the R code. 

# Support 
- For information you can mail to: [Linda's email](ld.de.vries@st.hanze.nl) and [Carlo's email](c.van.buiten@st.hanze.nl)

# Credits 
- https://bioinf.nl/~fennaf/thema08/
- Fenna Feenstra
- Classmates