---
title: "Corticosteroid"
author: "Carlo van Buiten, Linda de Vries"
date: "April 22th 2020"
output:
  pdf_document:
    number_sections: yes
  html_document:
    df_print: paged
linkcolor: blue
header-includes:
- \usepackage{longtable}
- \usepackage{hyperref}
---

\newpage

\tableofcontents

\newpage

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, cache = TRUE)
```

# Introduction
Corticosteroids are a class of steroid hormones that are produced in the adrenal cortex of vertebrates, in this research our focus lies on one of the two main classes of corticosteroids: glucocorticoids, a class of corticosteroids that affect the metabolism of glucose. It is worth mentioning that synthetic analogs of these molecules are also classified as corticosteroids.  
There are many medicines, like dexamethasone, that (partly) consist of corticosteroids and these medicines are used to treat a wide range of ailments, ranging from cancer to ashtma or even used as supplements for organ transplant operations.     
   
```{r, echo=FALSE, out.width='50%'}
knitr::include_graphics('glucocorticoid_model.png')
```
   
As seen in the model above the drug binds to the receptor to form the drug-receptor complex, which becomes activated drug-receptor complex, which in turn promotes the deactivation of receptor mRNA to produce less receptor.  

\newpage

# Methods

## The software model
To simulate the glucocorticoid dynamic from the model and the formula we used the deSolve R-library. Its implementation can be seen below.
```{r}
# Load deSolve library
library(deSolve)

# The 4 equations that simulate glucocorticoid dynamics
glucocorticoidDynamics <- function(t, state, parms) {
  with(as.list(c(state, parms)), 
  {     
    dRm <- ksyn_rm*(1-DRN/(IC50 + DRN)) - kd_Rm*Rm
    dR <- ks_r*Rm + Rf*kre*DRN - kon*D*R - kd_R*R
    dDR <- kon*D*R - kt*DR
    dDRN <- kt*DR - kre*DRN
    
    # Return the results
    list(c(dRm, dR, dDR, dDRN))
  })
}

# Starting states
state <- c(Rm = 4.74, # mRNA for the receptor
           R = 267,   # Receptor density
           DR = 0,    # Drug-receptor complex
           DRN = 0)   # Nuclear activated drug-receptor complex

# Calculation intervals
times <- seq(0, 48, by = 1)

# Load model parameters
parameters <- c(
  ksyn_rm = 2.9,
  IC50 = 26.2,
  kon = 0.00329,
  kt = 0.63,
  kre = 0.57, 
  Rf = 0.49,
  kd_R = 0.0572,
  kd_Rm = 0.612,
  ks_r = 3.22,
  D = 53.40868  # 20*1000/374.471
)

# Call ode and store the results in out
out <- as.data.frame(ode(
  y = state,
  times = times,
  func = glucocorticoidDynamics,
  parms = parameters
))
```

## Model configuration
\begin{longtable}[l]{l|l|l}
\caption{Parameters and values} \\ \hline
\label{param_val_table}
$\textbf{Parameter}$             &$\textbf{Values}$             &$\textbf{Unit}$              \\ \hline
\endhead
$k_{s\_Rm}$     & 2.90       & $fmol/g liver/h$       \\ \hline
$IC_{50\_Rm}$   & 26.2       & $fmol/mg protein$      \\ \hline
$k_{on}$        & 0.00329    & $L/nmol/h$             \\ \hline
$k_T$           & 0.63       & $h^{-1}$               \\ \hline
$k_{re}$        & 0.57       & $h^{-1}$               \\ \hline
$Rf$            & 0.49                                \\ \hline
$k_{d\_R}$      & 0.0572     & $h^{-1}$               \\ \hline
$k_{d\_Rm}$     & 0.612                               \\ \hline
$k_{s\_r}$      & 3.22                                \\ \hline
$D$             & 53.40868   & $nmol/L$               \\ \hline
\end{longtable}

\begin{longtable}[l]{l|l|l}
\caption{Initial Values} \\ \hline
\label{initial_val_table}
$\textbf{Parameter}$             &$\textbf{Values}$             &$\textbf{Unit}$              \\ \hline
\endhead
$R_{m0}$    & 4.47      & $(fmol/g liver)$         \\ \hline
$R_0$       & 267       & $(fmol/mg protein)$      \\ \hline
$DR$        & 0         & $(fmol/mg protein)$      \\ \hline
$DR(N)$     & 0         & $(fmol/mg protein)$      \\ \hline
\end{longtable}

\newpage

# Results
```{r}
# Make a plot of concentration of receptor mRNA and free receptor density
par(mfrow = c(2, 2))
  plot(out$time, out$Rm, ylim = c(0, 5), xlab = "Time", ylab = "Receptor mRNA", type = "l", lwd = 2)
  plot(out$time, out$R, ylim = c(0, 500), xlab = "Time", ylab = "Free receptor density", type = "l", lwd = 2)
  plot(out$time, out$DR, ylim = c(0, 50), xlab = "Time", ylab = "Drug-receptor complex", type = "l", lwd = 2)
  plot(out$time, out$DRN, ylim = c(0, 50), xlab = "Time", ylab = "Activated receptor complex", type = "l", lwd = 2)
```

As can be seen above, the mRNA receptor plot is almost the exact inverse of the drug-receptor complex plot, barring the slight delay between the first increase in drug-receptor complex and the first decrease in mRNA, an intermediate form of this delay can also be seen in the plot of the activated receptor complex. This is to be expected, because the drug-receptor complex turning into the activated receptor complex takes some time, and the deactivation of the receptor mRNA by the activated receptor complex is also not instantaneous. Over the 2 day span, a steady decline in free receptor can be observed, this makes sense, for the mRNA coding for the receptor also sees an overal decrease.  
For the drug to do its work, it is absolutely vital that there is enough free receptor for the drug to bind to and form the drug-receptor complex. Without any free receptors to bind to, the drug cannot form the activated steroid-receptor complex.
