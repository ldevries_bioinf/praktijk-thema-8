Authors: C. van Buiten, L. de Vries   
Date: 22 April 2020   
Version: Bio-informatica 2, praktijk opdracht 08   

# Project name 
Corticosteroid   

# Introduction 
Corticosteroids are a class of steroid hormones that are produced in the adrenal cortex of vertebrates, in this research our focus lies on one of the two main classes of corticosteroids: glucocorticoids, a class of corticosteroids that affect the metabolism of glucose. It is worth mentioning that synthetic analogs of these molecules are also classified as corticosteroids. There are many medicines, like dexamethasone, that (partly) consist of corticosteroids and these medicines are used to treat a wide range of ailments, ranging from cancer to ashtma or even used as supplements for organ transplant operations.  

# Requirements  
Rstudio_1.2.5019  
R_3.5.2   
deSolve_1.28  
See 'Manual' on how to install these packages.

# Installing
Clone the source files. Type this in the terminal ```git clone https://ldevries_bioinf@bitbucket.org/ldevries_bioinf/praktijk-thema-8.git``` or look at the files on the page https://bitbucket.org/ldevries_bioinf/praktijk-thema-8/src/master/week2_corticosteroid_opdracht/.

# Manual 
The folder praktijk-thema-8 contains the files you use yo get the results. You start with running the ```package_installer.Rmd``` file, to install all the packages you need. After that, the script ```carlo_393539_linda_382059.Rmd``` can be run. Here you can look at the results, there is also a report this is the PDF file ```carlo_393539_linda_382059.pdf```. There is also a script ```carlo_393539_linda_382059_code.R``` that only includes the R code that produces the plot ```plotmodel_MPL20.pdf```. 

# Support 
- For information you can mail to: [Linda's email](ld.de.vries@st.hanze.nl) and [Carlo's email](c.van.buiten@st.hanze.nl)

# Credits 
- https://bioinf.nl/~fennaf/thema08/
- Fenna Feenstra
- Classmates