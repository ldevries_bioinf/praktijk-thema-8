Authors: C. van Buiten, L. de Vries   
Date: July 11th 2020   
Version: Bio-informatica 2, praktijk opdracht 08   
 

# Project name 
COVID-19

# Introduction 
It was the 30th of december when dr. Li Wenliang warned everyone that the newly discovered strand of the corona virus, COVID-19, would turn into an epidemic. Fast forward to july 2020 and all around the globe countries have been hit by the corona-crisis. With almost everyone being struck by the effects of the virus outbreak and the absence of a vaccine, authorities deemed the only logical course of action to be for countries to put themselves in a lockdown. But while the effects of such a lockdown are largely unclear when it comes to the spread of the virus, it is clear that the lockdown greatly, and negatively, impacts the world economy. It is for this reason that it is especially important to be aware of the effects of a lockdown on the spread of a disease, so as to make an informed decision on whether (partly) sacrificing the economy is worth the potential gain in terms of health and safety.

# Prerequisites
The program requires the following packages to be installed:
- Rstudio_1.2.5019            
- R_3.5.2  
- deSolve_1.28
See 'Manual' on how to install these packages.

# Installing
Clone the source files. Type this in the terminal ```git clone https://ldevries_bioinf@bitbucket.org/ldevries_bioinf/praktijk-thema-8.git``` or look at the files and download it on the bitbucket page ```https://bitbucket.org/ldevries_bioinf/praktijk-thema-8/src/master/praktijk-thema-8-COVID-19/```.

# Manual 
The folder praktijk-thema-8-COVID-19 contains the files you use to get the results. In the directory from praktijk-thema-8-COVID-19 you can open the ```package_installer.Rmd``` file and start by running, to install all the packages you need. After that, the script ```carlo_393539_linda_382059_COVID-19_report.Rmd``` can be run but is not necessary, because it has already been run to generate the PDF. The output of that script is the PDF file: ```carlo_393539_linda_382059_COVID-19_report.pdf```. There is also a script ```carlo_393539_linda_382059_COVID-19_code.R``` this only includes the R code. However, for it to run properly it is required to update the dates in the script by simply adding the amount of days since July 11th to the ```tend``` of each simulation because everyday new COVID-19 data becomes available. 

# Support 
- For information you can mail to: [Carlo's email](c.van.buiten@st.hanze.nl) and [Linda's email](ld.de.vries@st.hanze.nl)

# Credits 
- https://bioinf.nl/~fennaf/thema08/
- Fenna Feenstra
- Sourish Das [https://arxiv.org/abs/2004.03147v1]