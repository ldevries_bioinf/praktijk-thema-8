---
title: "COVID-19"
author: "Carlo van Buiten, Linda de Vries"
date: "July 11th 2020"
output:
  pdf_document:
    number_sections: yes
  html_document:
    df_print: paged
linkcolor: blue
header-includes:
- \usepackage{longtable}
- \usepackage{hyperref}
---

```{r, echo=FALSE, out.width='100%', fig.cap="COVID-19 \\cite{COVID-19 image}", fig.align='center'}
knitr::include_graphics('covid-19-image.png')
```

\newpage

\tableofcontents

\newpage

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, cache = TRUE)
```

# Introduction
It was the 30th of december when dr. Li Wenliang warned everyone that the newly discovered strand of the corona virus, COVID-19, would turn into an epidemic. Fast forward to july 2020 and all around the globe countries have been hit by the corona-crisis. With almost everyone being struck by the effects of the virus outbreak and the absence of a vaccine, authorities deemed the only logical course of action to be for countries to put themselves in a lockdown. But while the effects of such a lockdown are largely unclear when it comes to the spread of the virus, it is clear that the lockdown greatly, and negatively, impacts the world economy. It is for this reason that it is especially important to be aware of the effects of a lockdown on the spread of a disease, so as to make an informed decision on whether (partly) sacrificing the economy is worth the potential gain in terms of health and safety.  
In essence, the question we are asking is: "What is the effect of a lockdown?" Or to put it bluntly: "Has the lockdown worked?" The report \cite{report} on which this is based was published on April 7th and therefore only took into account the data that was available up until that point, meaning it could only conclude the lockdown had not worked until then, according to the SIR model, as can be seen in the graph below. The report also specifically looked at India while we take a look at several other countries as well. This offers us the opportunity to compare the effects of different types of government policy. 

```{r, echo=FALSE, out.width='70%', fig.cap="COVID-19 spread in India until April 7th, compared to the prediction for the virus. \\cite{report}", fig.align='center'}
knitr::include_graphics('Covid19/lockdownindia.png')
```
The true (measured) data actually comes out above the SIR modelling for a default scenario without a lockdown, meaning the lockdown had not worked prior to April 7th. Now, with more data available, we can draw a more accurate and more definitive conclusion on the effect the lockdown has had.  
\newpage

The model that is used for this is the Susceptible, Infected, Recovered (SIR) model. This is a popular epidemic model for infectious diseases \cite{SIR model}.    

```{r, echo=FALSE, out.width='50%', fig.cap="SIR model for epidemics. \\cite{SIR model image}", fig.align='center'}
knitr::include_graphics('Covid19/SIR-SIRS.png')
```
The diagram above shows how individuals may be affected by the virus. With $\xi$ being the rate at which someone who has garnered immunity may lose this immunity and once more become susceptible to infection. This may or may not be equal to 0 (permanent immunity). The model assumes the infection rate to be constant and the people an individual encounters to be mixed homogenously \cite{SIR model}. This is almost never the case however, since population density and the time of year are just two of the many factors that impact the transmission rate. It also does not include births or deaths or migration\cite{SIR model}.
    
$$\frac{\delta S}{\delta t} = -\beta \frac{SI}{N}$$  
$$\frac{\delta I}{\delta t} = +\beta \frac{SI}{N} -\gamma I$$  
$$\frac{\delta R}{\delta t} =  +\gamma I$$  

\- $t$ it the time in days.  
- $S$ is the number of susceptible people.   
- $I$ is the number of infected people.     
- $R$ is the number of recovered people.    
- $\beta$ is the transmission rate.     
- $\gamma$ is the recovery rate.   
- $\gamma I$ is the flow out of the infected crowd and goes into the recovered group.   
\newpage

# Methods

## The software model
To simulate the SIR model equations we used the deSolve R-library. Its implementation can be seen below.
```{r, echo=TRUE}
require("deSolve")

# The SIR model function
SIR_function=function(t, x, vparameters){
  # SIR Parameters
  S = x[1]  # Susceptible
  I = x[2]  # Infected
  R = x[3]  # Recovered
  
  with(as.list(vparameters),{
    # Total population
    npop = S+I+R
    
    # The SIR model equations
    dS = -beta*S*I/npop            
    dI = +beta*S*I/npop - gamma*I  
    dR = +gamma*I                  
    
    # Output
    vout = c(dS,dI,dR)
    list(vout)
  })
}
```

\newpage

## Model configuration
While using the SIR model some parameters are required. In the tables below you can see all the parameters that have been used in this simulation. Some of them are already explained in the content introduction. The $npop$ is the country population. The $I$ is the initial amount of infected/contagious people, for the simulation we have chosen start dates for each country where these numbers line up as much as possible, so as to get similar estimations, because this value impacts the model a lot and using a start date where a country has only 1 or 2 confirmed cases would skew the comparison, especially so because it is likely that during the beginning phase of the disease spread, a lot of cases will be missed in the count. The $R_0$ is known as the Basic Reproduction Number. It supposes that during the epidemic, each infected person goes on to infect an average of $R_0$ other (susceptible) people, who then do the same. While the $R_0$ may differ between areas and time periods, the average $R_0$ for COVID-19 has been estimated between 2 and 2.5 \cite{R0 estimation}, we have opted for the most conservative approach between these boundaries by using an $R_0$ of 2 in our equations. The recovery time, $\gamma$, also seems to differ wildy between patients, but has been estimated to be an average of 14 days for mild cases \cite{Recovery time}.

\begin{longtable}[1]{l|l|l|l|l}
\caption{Parameters of India, Sweden and Japan} \\ \hline
\label{param_val_table1}
$\textbf{Parameter}$ & $\textbf{India}$ & $\textbf{Sweden}$ & $\textbf{Japan}$ & $\textbf{Unit}$ \\ \hline
\endhead
$npop$   & 1359772087     & 10099265       & 126476461      & $People$     \\ \hline
$I$      & 244            & 248            & 245            & $People$     \\ \hline
$R$      & 0              & 0              & 0              & $People$   \\ \hline
$S$      & $npop-I-R$     & $npop-I-R$     & $npop-I-R$     & $People$     \\ \hline
$R_0$    & 2              & 2              & 2              & $People$   \\ \hline
$\gamma$ & 1/14           & 1/14           & 1/14           & $d^{-1}$  \\ \hline
$\beta$  & $R_0 * \gamma$ & $R_0 * \gamma$ & $R_0 * \gamma$ & $Transmission\ rate$ \\ \hline
\end{longtable}

\begin{longtable}[2]{l|l|l|l}
\caption{Parameters of The Netherlands and United States} \\ \hline
\label{param_val_table2}
$\textbf{Parameter}$ & $\textbf{The Netherlands}$ & $\textbf{US}$ & $\textbf{Unit}$ \\ \hline
\endhead
$npop$   & 17134872       & 331049917      & $People$  \\ \hline
$I$      & 265            & 222            & $People$  \\ \hline
$R$      & 0              & 0              & $People$  \\ \hline
$S$      & $npop-I_0-R_0$ & $npop-I_0-R_0$ & $People$  \\ \hline
$R_0$    & 2              & 2              & $People$  \\ \hline
$\gamma$ & 1/14           & 1/14           & $d^{-1}$  \\ \hline
$\beta$  & $R_0 * \gamma$ & $R_0 * \gamma$ & $Transmission\ rate$ \\ \hline
\end{longtable}

\begin{longtable}[3]{l|l|l|l}
\caption{Parameters of Italy and China} \\ \hline
\label{param_val_table3}
$\textbf{Parameter}$ & $\textbf{Italy}$ & $\textbf{China}$ & $\textbf{Unit}$ \\ \hline
\endhead
$npop$   & 60461826       & 1439464621     & $People$     \\ \hline
$I$      & 229            & 548            & $People$     \\ \hline
$R$      & 0              & 0              & $People$   \\ \hline
$S$      & $npop-I_0-R_0$ & $npop-I_0-R_0$ & $People$     \\ \hline
$R_0$    & 2              & 2              & $People$   \\ \hline
$\gamma$ & 1/14           & 1/14           & $d^{-1}$  \\ \hline
$\beta$  & $R_0 * \gamma$ & $R_0 * \gamma$ & $Transmission\ rate$ \\ \hline
\end{longtable}
\newpage

# Results  
We have made SIR model predictions and comparisons to those predictions for seven countries, among these were countries with strict lockdowns like India, Italy and China, countries with less strict lockdowns like the USA, Japan and the Netherlands and a country with no lockdown at all: Sweden. Together, these graphs help us better understand the effect of a lockdown on the spread of a virus like COVID-19.   

```{r, echo=FALSE, out.width='60%', fig.cap="India compared to its SIR model prediction.", fig.align='center'}
knitr::include_graphics('Plots/India.pdf')
```
Of all the countries we have examined, the scenario in India looks the most like what the SIR model predicted, however, the reality is a bit more severe than what the model predicts with an $R_0$ of 2. This could mean that the real $R_0$ was higher than 2, but the curve seems to slow down more than that of the SIR prediction, so this would not explain everything. Another startling fact is that even if the real $R_0$ was slightly higher than 2 in India, the lockdown seems to have had no effect in slowing down the spread of the virus whatsoever, ending up with around 820.000 infected patients.   
\newpage

```{r, echo=FALSE, out.width='60%', fig.cap="Sweden compared to its SIR model prediction.", fig.align='center'}
knitr::include_graphics('Plots/Sweden.pdf')
```
Sweden, a country that imposed no lockdown at all, seems to be faring rather well when compared to its SIR model prediction, the virus spread being far from exponential, however, Sweden also has a very low population density, so the $R_0$ value of 2 is probably not accurate for the country as a whole, with the disease likely spreading quickly in cities, but slower from city to city. If this were the correct, however, you would expect the spread to slow down after the contaminated cities have reached their capacity, but the spread does seem to increase slightly near the end, ending up with around 75.000 infected patients.   
\newpage 

```{r, echo=FALSE, out.width='60%', fig.cap="Japan compared to its SIR model prediction.", fig.align='center'}
knitr::include_graphics('Plots/Japan.pdf')
```
Japan has incredibly low numbers, peaking at around 21.000 infected patients. Japan also seems to more clearly show a positive effect from their lockdown policy. Further cementing this idea is the fact that after the last of the policies has been lifted, the transmission rate seems to increase once more.   
\newpage

```{r, echo=FALSE, out.width='60%', fig.cap="The Netherlands compared to its SIR model prediction.", fig.align='center'}
knitr::include_graphics('Plots/Netherlands.pdf')
```
While the Netherlands seems to increase a lot faster than the model, from the lockdown on it seems to increase linearly as opposed to exponentially and after a while the spread even slows down. This slowdown appears to continue even after the lifting of some of the emergency rulings however, resulting in a total of 51.000 total infected citizens.   
\newpage

```{r, echo=FALSE, out.width='60%', fig.cap="US compared to its SIR model prediction.", fig.align='center'}
knitr::include_graphics('Plots/US.pdf')
```
The spread rate in the USA is an astonishing leap from the rate predicted by the model, peaking at well over 3 million, this almost doubles the 1.7 million prediction. This is despite the lockdown that was initiated in mid-march. Most worrying, perhaps, is the fact that near the start of July the infection rate seems to increase once more. This does not fall in line with the start and end dates of the lockdown, even when taking an incubation time of a couple of days into account. Lockdown in the USA seems to have had an adverse effect, if it had any effect at all.   
\newpage 

```{r, echo=FALSE, out.width='60%', fig.cap="Italy compared to its SIR model prediction.", fig.align='center'}
knitr::include_graphics('Plots/Italy.pdf')
```
The rate we see in Italy at the start of March all the way through to the middle of May is significantly higher than the model predicted. Howevever, the curve flattens almost completely near the end of May. This results in the total number of confirmed cases actually ending a lot lower than that which the model predicted. The model predicted over 3 million cases, whereas the real number of confirmed cases is less than a tenth at 240.000.   
\newpage

```{r, echo=FALSE, out.width='60%', fig.cap="China compared to its SIR model prediction.", fig.align='center'}
knitr::include_graphics('Plots/China.pdf')
```
China perhaps shows the most shocking results. The transmission rate is tremendous at the start, far higher than that of the model. What is more, however, is the fact that at around the middle of February, the increase in the amount of confirmed cases seems to come to an almost immediate halt.   
\newpage

# Discussion and Conclusion
India ended up with around 820.000 infected patients, which equates to roughly 0.06% of its population, while the model predicted roughly 100.000 less which is less than a 0.01% difference, but a difference nonetheless. 
It seems the $R_0$ was slightly higher than 2 in India which lead to a slightly higher curve of confirmed cases. The modeled curve falls in its shadow while retaining a similar shape however, indicating that the lockdown imposed by the Indian government was ineffective.  

While the intial transmission rate in Sweden is surprising, especially considering the population density, it seems that the lack of a lockdown has actually not been as detrimental to them as you would expect, ending at 75.000 cases while the model predicted around 960.000 cases. This equates to 0.7% of the population versus 9.5% of the population. This quite significant difference of 8.8% would have us believe that the lack of a lockdown has proven itself a boon. It is worth noting however that the initial infection rate is higher than the modeled rate in each of our analyzed countries and the eventual, far lower, infection rate could be at least partly caused by the low population density of Sweden as a whole.  

Japan, despite being one of the first countries to have at least one confirmed case of COVID-19 (going as far back as January), has actually performed best out of all the countries we have taken a look at. The infection rate closely following the modeled curve at the start, and then shortly after the countermeasures had been imposed by the Japanese government quickly slowing down, ending up at 21.000 infected people. This number almost falls into nothingness when compared with the countries population of 126 million people as this is roughly 0.02%. Especially when compared to the modeled prediction of 2.8 million which equates to 2.2%. Another striking fact about the curve of Japan is that the infection rate starts increasing again shortly after the last local lockdown measures are lifted. This is a strong indication that the Japanese approach to combatting the spread of COVID was an effective one.  

The Netherlands is another country that went into lockdown and while the initial rate is in fact higher than the model at the start of the graph, it does not increase exponentially from the start of the lockdown onwards. This could indicate that the $R_0$ was higher than 2 in the Netherlands and that the lockdown did in fact help slow down the infection rate from exponential to linear. The curve of the confirmed cases does start to flatten though, even continuing to do so throughout the reopening of numerous of the facilities across the country and easing several other lockdown restrictions. The total confirmed cases end up at around 51.000, which is around 0.3% of the population this stands in stark contrast with the 1.3 million, or 7.6% predicted by the model. Overall the graph of the Netherlands does seem to support the hypothesis that a lockdown helps in slowing down the spread of the virus.  

Some of the most incredible results come from the USA, ending up with almost double the infection rate predicted by the model despite initiating a lockdown around the middle of March; 3.2 million versus 1.76 million or 1% versus 0.5% of the population. While the increase is linear at first, it seems to pick up near the beginning of July (after briefly slowing down after the end of the lockdown), still continuing to increase to this day, which paints a worrying picture for the future of the country. One possible explanation for these incredible rates given the timeframe is the growing amount of uncertainty and fraud surrounding COVID in the USA \cite{US fraud}. While this does not proof anything conclusively, it does mean the numbers coming from America should be taken with a grain of salt.  

The amount of confirmed COVID-19 cases in Italy increased very rapidly from the end of February onwards, until around the start of April where it started to decline and then flatten at around the start of May. The original high infection rate can likely at least partly be chalked up to Italy handling the initial infection very poorly \cite{Italy incident}, potentially infecting a lot of unnecessary people. Italy peaked at 240.000, or 0.4% of the population, which, when compared to the 3.2 million or 5.3%, indicates that they managed to control the spread very well eventually.  
\newpage 

China shows a most peculiar curve, the infection spread very rapidly until it neared 80.000 confirmed cases in the middle of February at which point the curve almost completely flatted instantly, ending at 85.000 several months later. This is incredibly surprising, especially considering the size of the population and the fact that China is where the spread started. It has peaked at 0.006% of its population, which is absolutely stunning when compared to the modeled prediction of 5.5% (near 80 million cases). Another thing to note is that the sudden halt in virus transmission does not seem to align with the Wuhan lockdown that was quickly enacted.  

It is incredibly hard to conclude anything on the effectiveness of lockdowns as a concept in general, especially because the SIR model assumes a constant population with homegenous mixing \cite{SIR model}, which is not the case when people actively avoid one another. This is true with and without a lockdown. What this means is that culture can also heavily impact the spread of the virus, for if people are more brazen in going about their daily business as usual, they will encounter more other random people and therefore more closely approach the SIR model situation. So while one conclusion we could draw from our analyses is that the Japanese approach to a lockdown, which is a light one \cite{Japan lockdown}, is incredibly effective, we cannot know for certain that the Japanese culture is not the root cause of the containment of the virus in Japan. For example, sick people in Japan have been known to wear masks long before the COVID-19 epidemic even started.  
Whereas in the west physical contact with acquaintances is far more common, even kissing people on the check upon meeting. Not only are all of these things factors to the transmission rate of viruses, they can also change on a whim as seen in the Netherlands, where once kissing people on the cheek was commonplace and now frowned upon.  

What we can say is that all countries barring the USA and India seem to have the virus under control reasonably well based on their confirmed cases staying far below the model prediction for a baseline situation. An India style lockdown seems to have no effect at all, if not a detrimental one.  

As for the future, we think it will be important to closely monitor the cases of COVID-19 around the world, especially for countries where the transmission rate is still growing exponentially like India and the USA. It would also be wise to thoroughly examine the way countries like Japan and China managed to control the virus so succesfully.
\newpage

\begin{thebibliography}{9}
\bibitem{report}
\textit{COVID-19 Disease Progression}. Das. S. (2020). \textit{Cornell University: Prediction of COVID-19 Disease Progression in India : 'Under the Effect of National Lockdown.'}. consulted on 21 June 2020, from https://arxiv.org/abs/2004.03147v1

\bibitem{code source}
Das. S.  \textit{Research of Sourish}. 2020. \textit{sourish-cmi/Covid19. Github}. consulted on 21 June 2020. from https://github.com/sourish-cmi/Covid19

\bibitem{data repository}
\textit{Novel Coronavirus (COVID-19) Cases}, provided by JHU CSSE. (2020). \textit{CSSEGISandData/COVID-19. Github}. consulted on 21 June 2020, from https://github.com/CSSEGISandData/COVID-19

\bibitem{SIR model}
S. Towers. 2012. \textit{Epidemic modelling with compartmental models using r}.  consulted on 26 June 2020. from http://sherrytowers.com/2012/12/11/simple-epidemic-modelling-with-an-sir-model/ 

\bibitem{SIR model image}
2020. \textit{SIR and SIRS models — Typhoid Model documentation}. consulted on 9 July 2020. from https://institutefordiseasemodeling.github.io/Documentation/typhoid/model-sir.html

\bibitem{COVID-19 image}
2020. \textit{Start NL - Maastricht}. consulted on 10 July 2020. from https://grenzinfo.eu/emrm/nl/

\bibitem{R0 estimation}
Sanche, S., Lin, Y., Xu, C., Romero-Severson, E., Hengartner, N., Ke, R. (2020).\textit{High Contagiousness and Rapid Spread of Severe Acute Respiratory Syndrome Coronavirus 2}. Emerging Infectious Diseases, 26(7), 1470-1477. consulted on 11 July 2020. from https://dx.doi.org/10.3201/eid2607.200282.

\bibitem{Recovery time}
\textit{Report of the WHO-China Joint Mission on Coronavirus Disease 2019 (COVID-19)}. consulted on 11 July 2020. from https://www.who.int/docs/default-source/coronaviruse/who-china-joint-mission-on-covid-19-final-report.pdf

\bibitem{US fraud}
\textit{COVID-19 Fraud: Law Enforcement’s Response to Those Exploiting the Pandemic | Federal Bureau of Investigation}. consulted on 11 July 2020. from https://www.fbi.gov/news/testimony/covid-19-fraud-law-enforcements-response-to-those-exploiting-the-pandemic

\bibitem{Italy incident}
Angela Dewan, C. (2020). \textit{Italy scrambles to contain coronavirus after admitting hospital mess-up}. consulted on 11 July 2020. from https://edition.cnn.com/2020/02/25/europe/italy-coronavirus-backfoot-intl/index.html

\bibitem{Wuhan lockdown}
2020. \textit{When Wuhan's lockdown began and how long it lasted}. consulted on 11 July 2020. from https://www.standard.co.uk/news/world/wuhan-lockdown-start-date-how-long-a4409866.html

\bibitem{Japan lockdown}
Trade, F. 2020. \textit{CORONAVIRUS - De toestand in Japan | Flanders Trade}. consulted on 11 July 2020. from https://www.flandersinvestmentandtrade.com/export/nieuws/coronavirus-de-toestand-japan

\end{thebibliography}
