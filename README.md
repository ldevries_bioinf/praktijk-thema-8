Authors: C. van Buiten, L. de Vries   
Date:  2020   
Version: Bio-informatica 2, praktijk opdracht 08   
 

# Project name 
-  

# Introduction 
-

# Prerequisites
The program requires the following packages to be installed:
- Rstudio_1.2.5019            
- R_3.5.2  
- deSolve_1.28
- SessionInfo()
See 'Manual' on how to install these packages.

# Installing
Clone the source files. Type this in the terminal ```git clone https://ldevries_bioinf@bitbucket.org/ldevries_bioinf/praktijk-thema-8.git``` or look at the files on the page ``` ```.

# Manual 
The folder praktijk-thema-8 contains the files you use yo get the results. In the directory from week1_mRNA_assingment you can open the ```package_installer.Rmd``` file and start with running, to install all the packages you need. After that, the script ```carlo_393539_linda_382059.Rmd``` can be run. Here you can look at the results, there is also a report this is the PDF file ```carlo_393539_linda_382059.pdf```. There is also a script ```carlo_393539_linda_382059.R``` this is only include the R code. 

# Support 
- For information you can mail to: [Linda's email](ld.de.vries@st.hanze.nl) and [Carlo's email](c.van.buiten@st.hanze.nl)

# Credits 
- https://bioinf.nl/~fennaf/thema08/
- Fenna Feenstra
- Classmates